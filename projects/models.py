from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name


class Task(models.Model):
    project = models.ForeignKey(
        Project, related_name="project_tasks", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.name
