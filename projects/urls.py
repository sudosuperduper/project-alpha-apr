from django.urls import path
from .views import ProjectListView, UserLoginView, show_project


app_name = "projects"

urlpatterns = [
    path("<int:id>/", show_project, name="show_project"),
    path("", ProjectListView.as_view(), name="list_projects"),
    path("login/", UserLoginView.as_view(), name="login"),
]
