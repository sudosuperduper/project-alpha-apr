from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from .models import Project
from django.contrib.auth.views import LoginView
from django.shortcuts import get_object_or_404, render
from django.db.models import Count


def get_project_queryset(request):
    projects = Project.objects.filter(owner=request.user)
    projects = projects.annotate(total_tasks=Count("task"))
    return projects


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.task_set.all()  # Get all tasks associated with the project
    return render(
        request,
        "projects/project_detail.html",
        {"project": project, "tasks": tasks},
    )


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/project_list.html"
    context_object_name = "projects"

    def get_queryset(self):
        projects = super().get_queryset()
        projects = projects.filter(owner=self.request.user)
        # Annotate each project with the total number of tasks
        projects = projects.annotate(total_tasks=Count("tasks"))
        return projects


def list_projects(request):
    projects = Project.objects.all()
    return render(
        request, "projects/project_list.html", {"projects": projects}
    )


class UserLoginView(LoginView):
    template_name = "projects/login.html"
