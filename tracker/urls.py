from django.urls import include, path
from django.views.generic import RedirectView
from projects.views import list_projects

urlpatterns = [
    path(
        "", RedirectView.as_view(url="/projects/"), name="home"
    ),  # Add the "home" URL pattern
    path("projects/", include("projects.urls")),
    path("accounts/", include("accounts.urls")),
    path("projects/", list_projects, name="list_projects"),
]
